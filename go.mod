module gotoot

go 1.16

require (
	github.com/gotk3/gotk3 v0.5.2
	github.com/subchen/go-xmldom v1.1.2
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/oauth2 v0.0.0-20210615190721-d04028783cf1
)
