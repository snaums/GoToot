BUILDVARS=-X 'main.Version=git-$(shell git rev-list -1 HEAD)' -X 'main.BuildDate=$(shell date)' -X 'main.BuildWith=$(shell go version)'
GLADEVAR=-X 'main.GladeString=$(shell sed 's/\"/\\\"/g' ui/views.glade)'

all:
	go run main.go

build:
	go build -ldflags "-s -w $(BUILDVARS) $(GLADEVAR)" main.go

dep:
	go get github.com/bbrks/go-blurhash github.com/gotk3/gotk3 github.com/subchen/go-xmldom

doc:
	godoc -http=localhost:6060
