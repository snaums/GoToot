package toot

import (
    "log"
    "errors"
    "strconv"
)

func Conversations () ( []TootConversation, error ) {
    var c []TootConversation
    err := lc.SimpleRequest ( "conversations", "GET", &c )
    return c, err
}

func (c *TootConversation) Remove () error {
    if len(c.Id) == 0 {
        return errors.New ("Need a valid Conversation ID" )
    }

    err := lc.SimpleRequest ( "conversations/"+c.Id, "DELETE", nil )
    return err
}

func (c *TootConversation) MarkRead () error {
    if len(c.Id) == 0 {
        return errors.New ("Need a valid Conversation ID" )
    }

    err := lc.SimpleRequest ( "conversations/"+c.Id+"/read", "POST", c )
    return err
}

func TimelineList ( listId string, args ...string ) ( []TootStatus, error ) {
    return Timeline ( "list/" + listId, args... )
}

func TimelineTag ( tag string, args ...string ) ( []TootStatus, error ) {
    return Timeline ( "tag/" + tag, args... )
}

func Timeline ( tp string, args ...string ) ( []TootStatus, error ) {
    var s []TootStatus
    resp, err := lc.GetEx ( "timelines/" + tp, args... )
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &s )
    if err != nil {
        return nil, err
    }
    return s, nil
}

func TimelineEx ( tp string, mediaOnly bool, local bool, limit int, max string, since string, min string ) ( []TootStatus, error ) {
    var args []string = []string{ "limit=" + strconv.Itoa(limit) }
    if mediaOnly == true {
        args = append ( args, "only_media=true" )
    }
    if local == true {
        args = append ( args, "local=true" )
    }
    if max != "" {
        args = append ( args, "max_id=" + max )
    }
    if min != "" {
        args = append ( args, "min_id=" + min )
    }
    if since != "" {
        args = append ( args, "since_id" + since )
    }

    var s []TootStatus
    resp, err := lc.GetEx ( "timelines/" + tp, args... )
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &s )
    if err != nil {
        return nil, err
    }
    log.Printf("%d // %+v\n", len(s), s )
    return s, nil
}
