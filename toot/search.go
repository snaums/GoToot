package toot

import (
    "errors"
    "net/url"
    "strconv"
)

func _search ( q string, tp string, limit int, ar *url.Values ) ( *TootResult, error ) {
    var res TootResult
    if q == "" {
        return  nil, errors.New("Querystring can't be empty")
    }
    if tp == "" {
        tp = "statuses"
    }

    if limit <= 0 {
        limit = 20
    }

    resp, err := lc.GetEx ( "search", "q", q, "type", tp, "limit", strconv.Itoa ( limit ) )
    if err != nil {
        return nil, err
    }
    if resp.StatusCode != 200 {
        return nil, errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }
    err = lc.DecodeResponse ( resp, &res )
    if err != nil {
        return nil, err
    }

    return &res, nil
}

func Search ( q string, tp string, limit int, args *url.Values ) ( *TootResult, error ) {
    oldPrfx := lc.SetApiPrefix ( "/api/v2" )
    r, err := _search ( q, tp, limit, args )
    lc.SetApiPrefix ( oldPrfx )
    return r, err
}

func SearchAll ( q string ) ( *TootResult, error ) {
    r, err := Search ( q, "statuses,accounts,hashtags", 40, nil )
    if err != nil {
        return nil, err
    }
    return r, nil
}

func SearchStatuses ( q string ) ( *[]TootStatus, error ) {
    r, err := Search ( q, "statuses", 0, nil )
    if err != nil {
        return nil, err
    }
    return &r.Statuses, nil
}

func SearchAccounts ( q string ) ( *[]TootAccount, error ) {
    r, err := Search ( q, "accounts", 0, nil )
    if err != nil {
        return nil, err
    }
    return &r.Accounts, nil
}

func SearchHashtags ( q string ) ( *[]TootTag, error ) {
    r, err := Search ( q, "hashtags", 0, nil )
    if err != nil {
        return nil, err
    }
    return &r.Hashtags, nil
}

