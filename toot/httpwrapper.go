package toot

import (
    "io"
    "os"
    "fmt"
    "bytes"
    "errors"
    "strings"
    "net/url"
    "net/http"
    "path/filepath"
    "encoding/json"
    "mime/multipart"
)

type mockerBuilder struct {
    strings.Builder
}

func (m* mockerBuilder) Write(p []byte) (n int, err error) {
    m.append ( string(p) )
    return 0,nil
}

func (m* mockerBuilder) append ( args... string) {
    for _, str := range args {
        m.WriteString ( str );
    }
}

type libConfig struct {
    client *http.Client
    server string
    apiPrefix string
}
var lc libConfig


func (lc *libConfig) SetApiPrefix ( prfx string ) string {
    x := lc.apiPrefix
    lc.apiPrefix = prfx
    return x
}

func (lc libConfig) SimpleRequest ( rest string, method string, v interface{} ) error {
    resp, err := lc.doSimpleRequest ( rest, method )
    if err != nil {
        return err
    }

    if resp.StatusCode != 200 {
        fmt.Printf ( "Response: %+v\n", resp )
        return errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }

    dec := json.NewDecoder ( resp.Body )
    for dec.More() {
        err := dec.Decode ( &v )
        if err != nil {
            return err
        }
    }

    return nil
}

func (lc libConfig) doSimpleRequest ( rest string, method string ) ( *http.Response, error ) {
    if method == "GET" {
        return lc.Get( rest )
    } else if method == "DELETE" {
        return lc.Delete ( rest )
    } else if method == "POST" {
        return lc.Post ( rest )
    } else {
        return nil, errors.New ("invalid method: " + method)
    }
}

func (lc libConfig) GetEx ( rest string, args ...string ) ( *http.Response, error ) {
    v := url.Values{}
    var in bool = false
    var r string = rest
    for i := 0; i < len ( args ); i+=2 {
        if in == false {
            in = true
            r = rest + "?"
        }
        v.Add ( args[i], args[i+1] )
    }
    return lc.Get ( r + v.Encode() )
}

func (lc libConfig) Get ( rest string ) ( *http.Response, error ) {
    url := "https://" + lc.server + lc.apiPrefix + "/" + rest
    fmt.Println ("GET: " + url )
    return lc.client.Get ( url )
}

func (lc libConfig) Post ( rest string ) ( *http.Response, error ) {
    return lc.PostForm ( rest, url.Values{} )
}

func (lc libConfig) Delete ( rest string ) (*http.Response, error) {
    url := "https://" + lc.server + lc.apiPrefix + "/" + rest
    fmt.Println ("Delete: " + url )
    req, _ := http.NewRequest( "DELETE", url, nil )
    return lc.client.Do ( req )
}

func (lc libConfig) PostForm ( rest string, data url.Values ) (*http.Response, error) {
    url := "https://" + lc.server + lc.apiPrefix + "/" + rest
    fmt.Println ("Post(Form): " + url )
    return lc.client.PostForm ( url, data )
}

func (lc libConfig) PostJSON ( rest string, JSONstring string ) ( *http.Response, error ) {
    url := "https://" + lc.server + lc.apiPrefix + "/" + rest

    req, _ := http.NewRequest( "POST", url, bytes.NewBufferString(JSONstring) )
    fmt.Printf("Post: %+v\n", req.URL)
    req.Header.Set("Content-Type", "application/json")
    fmt.Printf("REQUEST: %+v\n", req)
    return lc.client.Do ( req )
}

func (lc libConfig) PostFile ( rest string, formData map[string]string, formFilename string, filename string ) ( *http.Response, error) {
    url := "https://" + lc.server + lc.apiPrefix + "/" + rest
    file, err := os.Open( filename )
    if err != nil {
        return nil, err
    }
    defer file.Close()

    body := &bytes.Buffer{}
    writer := multipart.NewWriter( body )
    part, err := writer.CreateFormFile( formFilename, filepath.Base( filename ) )
    if err != nil {
        return nil, err
    }
    _, err = io.Copy( part, file )
    if err != nil {
        return nil, err
    }
    for key, val := range formData {
      _ = writer.WriteField(key, val)
    }

    err = writer.Close()
    if err != nil {
        return nil, err
    }

    fmt.Println ("Post(Multipart/Form-Data): " + url )
    req, _ := http.NewRequest( "POST", url, body )
    fmt.Printf("URL: %+v\n", req.URL)
    req.Header.Set("Content-Type", writer.FormDataContentType())
    return lc.client.Do ( req )
}

func (lc libConfig) DecodeResponse ( resp *http.Response, v interface{} ) error {
    if resp.StatusCode != 200 {
        fmt.Printf ( "Response: %+v\n", resp )
        return errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }

    dec := json.NewDecoder ( resp.Body )
    for dec.More() {
        err := dec.Decode ( v )
        if err != nil {
            return err
        }
    }
    return nil
}
