package toot

import (
    "time"
)

type TootNotificationType int
const (
    Follow = 0
    Mention = 1
    Reblog = 2
    Favourite = 3
)
func (v TootNotificationType) ToStr () string {
    if v == Follow {
        return "follow"
    } else if v == Mention {
        return "mention"
    } else if v == Reblog {
        return "reblog"
    } else {
        return "favourite"
    }
}


type TootVisibility int
const (
    Public = 0
    Unlisted = 1
    Private = 2
    Direct = 3
)
func (v TootVisibility) ToStr () string {
    if v == Public {
        return "public"
    } else if v == Unlisted {
        return "unlisted"
    } else if v == Private {
        return "private"
    } else {
        return "direct"
    }
}

type TootAttachmentType int
const (
    Unknown = 0
    Image = 1
    Gifv = 2
    Video = 3
)
func (v TootAttachmentType) ToStr () string {
    if v == Unknown {
        return "unknown"
    } else if v == Image {
        return "image"
    } else if v == Gifv {
        return "gifv"
    } else {
        return "video"
    }
}

type TootApplication struct {
    Name string
    Website string
}

type TootAccount struct {
    Id string
    Username string
    Acct string
    DisplayName string      `json:"display_name"`
    Locked bool
    CreatedAt time.Time     `json:"created_at"`
    FollowersCount int      `json:"followers_count"`
    FollowingCount int      `json:"following_count"`
    StatusesCount int       `json:"statuses_count"`
    Note string
    Url string
    Avatar string
    AvatarStatic string     `json:"avatar_static"`
    Header string
    HeaderStatic string     `json:"header_static"`
    Emojis []TootEmoji
    Moved *TootAccount
    Fields []struct {
        Name string
        Value string
        VerifiedAt *time.Time
    }
    Source struct {
        Fields []struct {
            Name string
            Value string
        }
        Note string
    }
    Bot bool
}

type TootAttachment struct {
    Id string
    Type string
    Url string
    RemoteUrl string        `json:"remote_url"`
    PreviewUrl string       `json:"preview_url"`
    TextUrl string          `json:"text_url"`
    //Meta TootHash
    Description string
    LocalUrl string         // used by GoToot! for finding attachments to be uploaded
    Blurhash string
}

type TootCard struct {
    Url string
    Title string
    Description string
    Type string
    AuthorName string   `json:"author_name"`
    AuthorUrl string    `json:"author_url"`
    ProviderName string `json:"provider_name"`
    ProviderUrl string  `json:"provider_url"`
    Html string
    Width int
    Height int
    Image string    // URL to the preview thumbnail
    EmbedUrl string     `json:"embed_url"`
    Blurhash string
}

type TootConversation struct {
    Id string
    Unread bool
    Accounts []TootAccount
    LastStatus TootStatus   `json:"last_status"`
}

type TootContext struct {
    Ancestory []TootStatus
    Descendants []TootStatus
}

type TootEmoji struct {
    ShortCode       string      `json:"short_code"`
    StaticUrl       string      `json:"static_url"`
    Url             string
    VisibleInPicker bool        `json:"visible_in_picker"`
    Category        string
}

type TootError struct {
    ErrorText string            `json:"error"`
    ErrorDescription string     `json:"error_description"`
}

func (e *TootError) Error () string {
    if e.ErrorDescription == "" {
        return e.ErrorText
    }
    return e.ErrorText + ": " + e.ErrorDescription
}

type TootFilter struct {
    Id string
    Phrase string
    Context string
    ExpiresAt time.Time     `json:"expires_at"`
    Irreversible bool
    WholeWord bool          `json:"whole_word"`
}

type TootInstance struct {
    Uri string
    Title string
    Description string
    Email string
    Version string
    Thumbnail string
    //Urls TootHash
    //Stats TootHash
    Languages []string
    ContactAccount TootAccount      `json:"contact_account"`
}

type TootList struct {
    Id string
    Title string
}

type TootMention struct {
    Url string
    Username string
    Acct string
    Id string
}

type TootNotification struct {
    Id string
    Type string
    CreatedAt time.Time     `json:"created_at"`
    Account TootAccount
    Status *TootStatus
}

type TootPoll struct {
    Id string
    ExpiresAt time.Time     `json:"expires_at"`
    Expired bool
    Multiple bool
    VotesCount int          `json:"votes_count"`
    Options []TootPollOptions
    Voted bool
    HideTotals bool
}

type TootPollOptions struct {
    Title string
    VotesCount int          `json:"votes_count"`
}

type TootRelationship struct {
    Id string
    Following bool
    FollowedBy bool         `json:"followed_by"`
    Blocking bool
    Muting bool
    MutingNotification bool     `json:"muting_notification"`
    Requested bool
    DomainBlocking bool         `json:"domain_blocking"`
    ShowingReblogs bool         `json:"showing_reblogs"`
    Endorsed bool
}

type TootResult struct {
    Accounts []TootAccount
    Statuses []TootStatus
    Hashtags []TootTag
}

type TootStatus struct {
    Id string
    Uri string
    Url string
    Account *TootAccount
    InReplyToId string          `json:"in_reply_to_id"`
    InReplyToAccountId string   `json:"in_reply_to_account_id"`
    Reblog *TootStatus
    Content string
    CreatedAt time.Time         `json:"created_at"`
    Emojis []TootEmoji
    RepliesCount int            `json:"replies_count"`
    ReblogsCount int            `json:"reblogs_count"`
    FavouritesCount int         `json:"favourites_count"`
    Reblogged bool
    Favourited bool
    Muted bool
    Sensitive bool
    SpoilerText string          `json:"spoiler_text"`
    Visibility string           // one of: public, unlisted, private, direct
    MediaAttachments []TootAttachment   `json:"media_attachments"`
    Mentions         []TootMention
    Tags             []TootTag
    Card             *TootCard
    Poll             *TootPoll
    Application      TootApplication
    Language         string
    Pinned           bool
}

type TootScheduledStatus struct {
    Id string
    ScheduledAt time.Time       `json:"scheduled_at"`
    Params struct {
        Poll *TootPoll
        Text    string
        MediaIds string         `json:"media_ids"`
        Sensitive   bool
        Visibility  string
        Idempotency     string
        ScheduledAt     string
        SpoilerText string          `json:"spoiler_text"`
        ApplicationId   int         `json:"application_id"`
        InReplyToId string          `json:"in_reply_to_id"`
    }
    MediaAttachments []TootAttachment   `json:"media_attachments"`
}

type TootTag struct {
    Name string
    Url string
    History []struct {
        Day string
        Uses string
        Accounts string
    }
}
