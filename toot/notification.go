package toot

import (
    "errors"
)

func Notifications () ( []TootNotification, error ) {
    var n []TootNotification
    err := lc.SimpleRequest ( "notifications", "GET", &n )
    return n, err
}

func (n *TootNotification) Get () error {
    if len ( n.Id ) <= 0 {
        return errors.New ( "Need an ID")
    }
    var no TootNotification
    err := lc.SimpleRequest ( "notifications/" + n.Id, "GET", &no )
    if err == nil {
        *n = no
    }
    return err
}

func (n *TootNotification) Dismiss () error {
    if len ( n.Id ) <= 0 {
        return errors.New ( "Need an ID")
    }
    err := lc.SimpleRequest ( "notifications/" + n.Id + "/dismiss", "POST", nil )
    return err
}

func NotificationsDismissAll () error {
    err := lc.SimpleRequest ( "notifications/clear", "POST", nil )
    return err
}
