package toot

import (
    "errors"
    "net/url"
    "strconv"
    "log"
)

func (acc TootAccount) WrapPost ( url string, pl interface{} ) error {
    if len(acc.Id) <= 0 {
        return errors.New ( "Need a valid Account ID" )
    }

    resp, err := lc.Post ( url )
    log.Println ( resp )
    if err != nil {
        return err
    }
    if resp.StatusCode != 200 {
        return errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }
    err = lc.DecodeResponse ( resp, pl )
    if err != nil {
        return err
    }

    return nil
}

func Blocks () ( []TootAccount, error ) {
    var s []TootAccount
    err := lc.SimpleRequest ( "blocks", "GET", &s )
    return s, err
}

func Mutes() ( []TootAccount, error ) {
    var s []TootAccount
    err := lc.SimpleRequest ( "mutes", "GET", &s )
    return s, err
}

func (acc TootAccount) WrapGet ( url string, pl interface{} ) error {
    if len(acc.Id) <= 0 {
        return errors.New ( "Need a valid Account ID" )
    }

    resp, err := lc.Get ( url )
    if err != nil {
        return err
    }
    if resp.StatusCode != 200 {
        return errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }
    err = lc.DecodeResponse ( resp, pl )
    if err != nil {
        return err
    }

    return nil
}

func FollowRequests () ([]TootAccount, error ) {
    var accs []TootAccount
    err := lc.SimpleRequest ( "follow_requests", "GET", &accs )
    return accs, err
}

func (acc TootAccount) FollowRequestsAuthorize () error {
    if len(acc.Id) == 0 {
        return errors.New ( "Need a valid Account ID" )
    }

    err := lc.SimpleRequest ( "follow_requests/"+acc.Id+"/authorize", "POST", nil )
    return err
}

func (acc TootAccount) FollowRequestsReject () error {
    if len(acc.Id) == 0 {
        return errors.New ( "Need a valid Account ID" )
    }

    err := lc.SimpleRequest ( "follow_requests/"+acc.Id+"/reject", "POST", nil )
    return err
}

func Relationships ( ids ...string) ( []TootRelationship, error ) {
    var rel []TootRelationship
    u := url.Values{}
    for _, i := range ids {
        u.Add ( "id[]", i )
    }
    var rest string = "accounts/relationships?" + u.Encode()
    err := lc.SimpleRequest ( rest, "GET", &rel )
    return rel, err
}

func (acc TootAccount) Relationship () ( TootRelationship, error ) {
    r, err := Relationships ( acc.Id )
    if len(r) >= 1 {
        return r[0], err
    }
    return TootRelationship{}, err
}

func (acc TootAccount) Search ( q string, limit int, resolve bool, following bool ) ([]TootAccount, error) {
    var accs []TootAccount
    if limit <= 0 {
        limit = 30
    }
    resp, err := lc.GetEx (
        "accounts/search",
        "q", url.QueryEscape(q),
        "limit", strconv.Itoa(limit),
        "resolve", strconv.FormatBool(resolve),
        "following", strconv.FormatBool(following),
    )
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &accs )
    if err != nil {
        return nil, err
    }
    return accs, err
}

func (acc TootAccount) PostNote ( comment string ) error {
    data := url.Values{}
    data.Add ( "comment", comment )
    resp, err := lc.PostForm ( "accounts/"+acc.Id+"/note", data )
    if err != nil {
        return err
    }

    if resp.StatusCode != 200 {
        return errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }
    return nil
}

func (acc TootAccount) Block () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/block", &r )
    return &r, err
}
func (acc TootAccount) Unblock () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/unblock", &r )
    return &r, err
}
func (acc TootAccount) Unmute () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/unmute", &r )
    return &r, err
}
func (acc TootAccount) Mute () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/mute", &r )
    return &r, err
}
func (acc TootAccount) Follow () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/follow", &r )
    return &r, err
}
func (acc TootAccount) Unfollow () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/unfollow", &r )
    return &r, err
}
func (acc TootAccount) Pin () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/pin", &r )
    return &r, err
}
func (acc TootAccount) Unpin () (*TootRelationship, error) {
    var r TootRelationship
    err := acc.WrapPost ( "accounts/"+acc.Id+"/unpin", &r )
    return &r, err
}

func (acc TootAccount) Statuses ( MediaOnly bool ) ([]TootStatus, error ) {
    if len(acc.Id) == 0 {
        return nil, errors.New ( "Need a valid Account ID" )
    }

    var statuses []TootStatus
    resp, err := lc.GetEx ( "accounts/" + acc.Id + "/statuses", "only_media", strconv.FormatBool(MediaOnly))
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &statuses )
    if err != nil {
        return nil, err
    }
    return statuses, err
}

func (acc TootAccount) Following () ([]TootAccount, error ) {
    if len(acc.Id) == 0 {
        return nil, errors.New ( "Need a valid Account ID" )
    }

    var accs []TootAccount
    resp, err := lc.Get ( "accounts/" + acc.Id + "/following" )
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &accs )
    if err != nil {
        return nil, err
    }
    return accs, err
}

func (acc TootAccount) Followers () ([]TootAccount, error ) {
    if len(acc.Id) == 0 {
        return nil, errors.New ( "Need a valid Account ID" )
    }

    var accs []TootAccount
    resp, err := lc.Get ( "accounts/" + acc.Id + "/followers" )
    if err != nil {
        return nil, err
    }

    err = lc.DecodeResponse ( resp, &accs )
    if err != nil {
        return nil, err
    }
    return accs, err
}

func (acc TootAccount) Get () (*TootAccount, error) {
    if len(acc.Id) == 0 {
        return nil, errors.New ( "Need a valid Account ID" )
    }
    err := lc.SimpleRequest ( "accounts/" + acc.Id, "GET", &acc )
    return &acc, err
}

func VerifyCredentials () (*TootAccount, error) {
    var acc TootAccount = TootAccount{}
    err := lc.SimpleRequest("accounts/verify_credentials", "GET", &acc )
    return &acc, err
}
