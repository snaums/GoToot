package toot

import (
    "os"
    "fmt"
    "io/ioutil"
    "encoding/json"
    "golang.org/x/oauth2"
)

type ConfigType struct {
    Token oauth2.Token
    ClientId string
    ClientSecret string
    Server string
    Scopes []string

    Filename string
}

func ReadConfig ( filename string ) ( ConfigType, error ) {
    var cfg ConfigType
    cfg.Filename = filename
    f, err := os.Open ( filename )
    if err != nil {
        return cfg, err
    }
    defer f.Close();

    dec := json.NewDecoder ( f )
    for dec.More() {
        err := dec.Decode ( &cfg )
        if err != nil {
            return ConfigType{ Filename: filename }, err
        }
    }

    cfg.Filename = filename
    return cfg, nil;
}

func (cfg ConfigType) Write ( filename string ) error {
    fmt.Println ( filename )
    output, err := json.MarshalIndent ( cfg , "", "  ")
    if err != nil {
        return err
    }
    err = ioutil.WriteFile( filename, output, 0644 )
    return err
}

