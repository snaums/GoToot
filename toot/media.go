package toot

func (a *TootAttachment) GetDescription () string {
    return a.Description
}

func (a *TootAttachment) SetDescription ( d string ) {
    a.Description = d
}

func (a *TootAttachment) GetFilename () string {
    return a.LocalUrl
}

func (a *TootAttachment) SetFilename ( f string ) {
    a.LocalUrl = f
}

func (a *TootAttachment) Post () ( *TootAttachment, error ) {
    if len(a.Id) > 0 {
        return a.Update()
    }

    var formData map[string]string = make(map[string]string)
    if len(a.Description) > 0 {
        formData["description"] = a.Description
    }

    // TODO: focal points
    resp, err := lc.PostFile ("media", formData, "file", a.LocalUrl )
    if err != nil {
        return nil, err
    }

    var att TootAttachment
    err = lc.DecodeResponse ( resp, &att )
    if err != nil {
        return nil, err
    }

    return &att, nil
}

func (a *TootAttachment) Update () ( *TootAttachment, error ) {
    return nil, nil
}
