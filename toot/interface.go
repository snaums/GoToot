package toot

type MediaInterface interface {
    Post ()             (*MediaInterface, error)
    Update ()           (*MediaInterface, error)
    SetDescription ( string )
    GetDescription ()   string
    SetFilename ( string )
    GetFilename ()      string
    Download ()         (string, error)
}

type StatusInterface interface {
    Get ()              (*StatusInterface, error)
    Context ()          (*ContextInterface, error)
    Card ()             (*CardInterface, error)
    RebloggedBy ()      ([]AccountInterface, error)
    FavouritedBy ()     ([]AccountInterface, error)
    PostStatus ()       (*StatusInterface, error)
    DeleteStatus ()     error
    PostReblog ()       (*StatusInterface, error)
    PostUnreblog ()     (*StatusInterface, error)
    PostPin ()          (*StatusInterface, error)
    PostUnpin ()        (*StatusInterface, error)
    PostFavourite ()    (*StatusInterface, error)
    PostUnfavourite ()  (*StatusInterface, error)
}

type InstanceInterface interface {
    Get ()          (InstanceInterface, error)
}

type CardInterface interface {

}

type ContextInterface interface {

}

type RelationshipInterface interface {

}

type AccountInterface interface {
    Get () (*AccountInterface, error)
    PostCreate ( string, string, string, bool, string ) (*AccountInterface, error)
    //VerifyCredentials () (AccountInterface, error)
    //UpdateCredentials ( string, string, string, string, bool, string, string, string ) Account
    Followers () ([]AccountInterface, error)
    Following () ([]AccountInterface, error)
    Statuses () ([]StatusInterface, error)

    Favourites () ([]StatusInterface, error)

    Follow ( AccountInterface ) (*RelationshipInterface, error)
    Unfollow ( AccountInterface ) (*RelationshipInterface, error)
    Relationships ( ...string ) ([]RelationshipInterface, error)
    Search ( string, int, bool, bool ) ([]AccountInterface, error)

    FollowRequests () ([]AccountInterface, error)
    FollowRequestAuthorize () error
    FollowRequestReject ()    error
}


