package toot

import (
    "fmt"
    "errors"
    "context"
    "net/url"
    "net/http"
    "encoding/json"
    "golang.org/x/oauth2"
)

// Name of the App for registering a new app at the server
const AppName = "GoToot!"
// website of the App for registering a new app at the server
const AppWebsite = "https://www.snaums.de"

// Struct for OAuth2 credentials received by the server on app-creation
type OAuth2Cred struct {
    Client_id string
    Client_secret string
}

// create a new app at the server and get OAuth2 credentials in return
func CreateOAuth2Credentials ( server string ) (string, string, error )  {
    resp, err := http.PostForm( "https://" + server + "/api/v1/apps",
	                url.Values {
                        "client_name": {AppName},
                        "redirect_uris": {"urn:ietf:wg:oauth:2.0:oob"},
                        "scopes": {"read write follow push"},
                        "website": {AppWebsite}})
    if err != nil {
        return "", "", err
    }
    if resp.StatusCode != 200 {
        return "", "", errors.New ("Statuscode " + string(resp.StatusCode) + " != 200 :/")
    }
    var o OAuth2Cred
    dec := json.NewDecoder ( resp.Body )
    for dec.More() {
        err := dec.Decode ( &o )
        if err != nil {
            return "", "", err
        }
    }

    return o.Client_id, o.Client_secret, nil
}

// set client settings for use in the toot-library
func SetActiveConf ( server string, client *http.Client, apiPrefix string ) {
    lc.server = server
    lc.client = client
    lc.apiPrefix = "/api/v1"
}

// internal function to create the configuration for the OAuth2 library
func createConf ( server string, clientID string, clientSecret string, scopes []string ) *oauth2.Config {
    return &oauth2.Config{
        ClientID:     clientID,
        ClientSecret: clientSecret,
        Scopes:       scopes,
        RedirectURL:  "urn:ietf:wg:oauth:2.0:oob",
        Endpoint: oauth2.Endpoint{
            AuthURL:  "https://"+server+"/oauth/authorize",
            TokenURL: "https://"+server+"/oauth/token",
        },
    }
}

// returns the OAuth2 authentification url, the user has to visit
func AuthUrl ( server string, clientID string, clientSecret string, scopes []string ) string {
    conf := createConf ( server, clientID, clientSecret, scopes )
    url := conf.AuthCodeURL("", oauth2.AccessTypeOnline)
    return url
}

// create a client-connection with a (hopefully) valid OAuth2 Auth Token
func ConnectWithToken ( server string, clientID string, clientSecret string, scopes []string, tok *oauth2.Token ) (*http.Client) {
    ctx := context.Background()
    conf := createConf ( server, clientID, clientSecret, scopes )

    client := conf.Client(ctx, tok)
    return client
}

// create a client-connection with an auth-code from the user, to get a token
func Connect ( server string, clientID string, clientSecret string, scopes []string, authCode string ) (*http.Client, *oauth2.Token, error) {
    ctx := context.Background()
    conf := createConf ( server, clientID, clientSecret, scopes )

    tok, err := conf.Exchange(ctx, authCode)
    if err != nil {
        //die()
        fmt.Printf("Error: %+v\n", err)
        return nil, nil, err
    } else {
        client := conf.Client(ctx, tok)
        return client, tok, nil
    }
}


