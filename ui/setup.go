
package ui

import (
    "log"
    "fmt"
    "strings"

    . "gotoot/toot"

	"github.com/gotk3/gotk3/gtk"
)

type SetupWindow struct {
    App *gtk.Application
    Window *gtk.Assistant

    Finished bool

    InstancePage *gtk.Box
    InstanceTestBtn *gtk.Button
    InstanceInput *gtk.Entry
    InstanceMessageField *gtk.Label

    AuthCodePage *gtk.Box
    AuthCodeTestBtn *gtk.Button
    AuthCodeInput *gtk.Entry
    AuthCodeMessageField *gtk.Label
    AuthCodeLink *gtk.Label

    FinishedPage *gtk.Label
}

var SetupWin SetupWindow

func SetupInit ( builder *gtk.Builder, cfg *ConfigType ) (*SetupWindow) {
    // identify all the needed parts of the setup window
    // will panic on type mismatch
    obj, _ := builder.GetObject ("SetupAssistant")
    SetupWin.Window = obj.(*gtk.Assistant)

    obj, _ = builder.GetObject ("SetupInstanceEntry")
    SetupWin.InstanceInput = obj.(*gtk.Entry)
    obj, _ = builder.GetObject ("SetupInstanceTestBtn")
    SetupWin.InstanceTestBtn = obj.(*gtk.Button)

    obj, _ = builder.GetObject ("SetupAuthCodeEntry")
    SetupWin.AuthCodeInput = obj.(*gtk.Entry)
    obj, _ = builder.GetObject ("SetupAuthCodeTestBtn")
    SetupWin.AuthCodeTestBtn = obj.(*gtk.Button)
    obj, _ = builder.GetObject ("SetupAuthCodeLink")
    SetupWin.AuthCodeLink = obj.(*gtk.Label)

    SetupWin.Finished = false

    SetupWin.Window.Connect ("cancel", func () {
        log.Println ("Cancelled")
        SetupWin.App.RemoveWindow ( SetupWin.Window )
        SetupWin.Window.Hide()
    })

    SetupWin.Window.Connect ("close", func () {
        log.Println("Closed")
        SetupWin.App.RemoveWindow ( SetupWin.Window )
        SetupWin.Window.Hide()
    })

    SetupWin.InstanceTestBtn.Connect ("clicked", func () {
        page, err := SetupWin.Window.GetNthPage(SetupWin.Window.GetCurrentPage())
        NextPage, err2 := SetupWin.Window.GetNthPage(SetupWin.Window.GetCurrentPage()+1)
        if err != nil || err2 != nil {
            // TODO: panic
            fmt.Println ("Cannot get page?")
            return
        }
        SetupWin.Window.SetPageComplete ( NextPage, false )
        SetupWin.Window.SetPageComplete ( page, false )

        v, err := SetupWin.InstanceInput.GetText()
        if err != nil {
            // TODO: fehler ausgeben
            fmt.Println ("Cannot get text?")
            return
        }
        fmt.Println ("Text of input: ", v)
        // TODO sanity check input
        cId, cSecret, err := CreateOAuth2Credentials ( v )

        if err == nil {
            SetupWin.Window.SetPageComplete ( page, true )

            cfg.Server = v
            cfg.ClientId = cId
            cfg.ClientSecret = cSecret
            cfg.Scopes = []string{"read write follow push"}
            url := AuthUrl ( cfg.Server, cfg.ClientId, cfg.ClientSecret, cfg.Scopes )
            SetupWin.AuthCodeLink.SetMarkup ("<a href=\"" + strings.ReplaceAll(url, "&", "&amp;") + "\" title=\"Instance Auth Link\">Instance Auth Link</a>" )
            SetupWin.Window.NextPage()
        } else {
            fmt.Println ("Secret and ID are not valid: ", err)
        }
    })

    SetupWin.AuthCodeTestBtn.Connect ("clicked", func () {
        page, err := SetupWin.Window.GetNthPage(SetupWin.Window.GetCurrentPage())
        NextPage, err2 := SetupWin.Window.GetNthPage(SetupWin.Window.GetCurrentPage()+1)
        if err != nil || err2 != nil {
            // TODO: panic
            fmt.Println ("Cannot get page?")
            return
        }
        SetupWin.Window.SetPageComplete ( NextPage, true )
        SetupWin.Window.SetPageComplete ( page, false )

        v, err := SetupWin.AuthCodeInput.GetText()
        if err != nil {
            // TODO: fehler ausgeben
            fmt.Println ("Cannot get text?")
            return
        }
        fmt.Println ("Text of input: ", v)
        // TODO sanity check input
        client, tok, err := Connect (
                                cfg.Server,
                                cfg.ClientId,
                                cfg.ClientSecret,
                                cfg.Scopes,
                                v )

        if err != nil {
            fmt.Printf ("Conection error: %v", err)
            panic("")
        }

        SetActiveConf ( cfg.Server, client, "/api/v1" )
        _, err = VerifyCredentials()
        if err != nil {
            fmt.Printf ("Token error: %v", err)
            panic("")
        }

        cfg.Token = *tok
        err = cfg.Write ( cfg.Filename )
        if err != nil {
            fmt.Printf ( "Config Write Error: %s -- %v\n", cfg.Filename, err )
        }
        SetupWin.Window.SetPageComplete ( page, true )
        SetupWin.Window.NextPage()
    })
    return &SetupWin
}

