package ui

import (
    . "gotoot/toot"

    "os"
    "log"

	"github.com/gotk3/gotk3/gtk"
	"github.com/gotk3/gotk3/glib"
)

var application *gtk.Application
var cache *Cache

const ConfigFile string = "gotootoot.cfg"
func GtkStart() {
    gtk.Init(nil)

    cfg,_  := ReadConfig ( ConfigFile )
    log.Println ("Config filename ", cfg.Filename )

    var err error
	application, err = gtk.ApplicationNew(appId, glib.APPLICATION_FLAGS_NONE)
    if err != nil {
        log.Println("Could not create the application, sorry mate.")
    }

	application.Connect("startup", func() {
		log.Println("application startup")

        cache, err = NewCacherFromFile ( "cache.persist" )
        if err != nil {
            log.Println ("Cacher: ", err)
        }

        GtkBuilder, err := createGtkBuilder ();
        if err != nil {
            log.Println ("Could not create Gtk Builder")
            return
        }
        SetupWin = *SetupInit ( GtkBuilder, &cfg )
        NewTootWin = NewTootForm( GtkBuilder )
        MainWin = MainWindowInit ( GtkBuilder )

        application.AddWindow ( SetupWin.Window )
        application.AddWindow ( NewTootWin.Window )
        application.AddWindow ( MainWin.Window )
        win, _ := MainWin.Window.GetWindow()
        NewTootWin.Window.SetParentWindow ( win )
    })

    application.Connect ("window-removed", func() {
        log.Println("application window-removed")

        if SetupWin.Finished == true {
            MainWin.Window.Show()
        }
    })

	// Connect function to application activate event
	application.Connect("activate", func() {
		log.Println("application activate")
        
        client, err := NewConnectionStartup ( &cfg )
        err = HandleError ( err )
        if client != nil  && err == nil {
            MainWin.Window.Show()
            log.Println ("Show main window")
        }
		// Get the GtkBuilder UI definition in the glade file.
    })

	// Connect function to application shutdown event, this is not required.
	application.Connect("shutdown", func() {
		log.Println("application shutdown")
	})

    // Launch the application
        //gtk.Main();
    os.Exit(application.Run(os.Args))
}

