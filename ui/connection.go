package ui

import (
    "net/http"
    . "gotoot/toot"
)

// FollowUp Values
const (
    Ignore = iota
    ShowSetup = iota
    ShowModal = iota
)

type tootError struct {
    Msg string
    FollowUp int
}

func (e *tootError) Error () string {
    return e.Msg
}

func NewError ( msg string, followUp int ) *tootError {
    return &tootError{Msg: msg, FollowUp: followUp}
}

func NewConnectionStartup ( cfg *ConfigType ) (*http.Client, error) {
    var client *http.Client = nil

    if cfg.Server == "" || cfg.ClientId == "" || cfg.ClientSecret == "" {
        return nil, NewError ( "Missing OAuth Information", ShowSetup );
    }

    client = ConnectWithToken ( cfg.Server, cfg.ClientId, cfg.ClientSecret, cfg.Scopes, &cfg.Token )
    SetActiveConf ( cfg.Server, client, "/api/v1" )
    _, err := VerifyCredentials()
    if err != nil {
        //fmt.Printf ("Config-token error: %v", err)
        return nil, NewError ( err.Error(), ShowSetup );
    }

    return client, nil
}

