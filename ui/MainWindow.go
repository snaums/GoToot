package ui

import (
  . "gotoot/toot"
    "log"

    "github.com/gotk3/gotk3/gtk"
    "github.com/gotk3/gotk3/glib"
)

var MainWin *MainWindow
type MainWindow struct {
    Window          *gtk.Window
    WindowWidth     int
    WindowHeight    int
    App             *gtk.Application

    Notebook        *gtk.Notebook

    SendBtn         *gtk.Button
    HomeBox         *gtk.Box
    LocalBox        *gtk.Box
    ConversationBox *gtk.Box
    NotificationsBox *gtk.Box

    HeaderBar       *gtk.HeaderBar

    Account         AccountView
    OtherAccountLabel *gtk.Label
    OtherAccount    AccountView
    Search struct {
        Entry       *gtk.Entry
        Button      *gtk.Button

        Toots       *gtk.Box
        Accounts    *gtk.Box
        Hashtags    *gtk.Box
    }
}

const (
    TootPage = 0
    MediaPage = 1
    FollowersPage = 2
    FollowingPage = 3
    FollowRequestsPage = 4
)

const (
    TimelineHomePage = 0
    TimelineLocalPage = 1
    ConversationsPage = 2
    NotificationsPage = 3
    TootDetailPage = 4
    AccountPage = 5
    OtherAccountPage = 6
    SearchPage = 7
)

func MainWindowMainNotebook ( num int ) {
    tootMap = make(map[string]TootUi)
    if num != OtherAccountPage {
        w, _ := MainWin.Notebook.GetNthPage ( OtherAccountPage )
        w.ToWidget().Hide()
    }
    if num != TootDetailPage {
        w, _ := MainWin.Notebook.GetNthPage ( TootDetailPage )
        w.ToWidget().Hide()
    }
    switch num {
        case TimelineHomePage:
            ph := PlaceHolder ( MainWin.HomeBox )
            go func () {
                toots, err := Timeline ( "home" )
                if err != nil {
                    return
                }
                glib.IdleAdd ( func () {
                    TootListing ( MainWin.HomeBox, toots, ph )
                })
            }()
        case ConversationsPage:
            ph := PlaceHolder ( MainWin.ConversationBox )
            go func () {
                conv, err := Conversations ()
                if err != nil {
                    log.Println (err )
                    return
                }
                log.Println ( "Conversations: ", conv )
                var toots []TootStatus
                for _, c := range conv {
                    toots = append ( toots, c.LastStatus )
                }
                glib.IdleAdd ( func () {
                    TootListing ( MainWin.ConversationBox, toots, ph )
                })
            }()
        case NotificationsPage:
            ph := PlaceHolder ( MainWin.NotificationsBox )
            go func () {
                noti, err := Notifications ()
                if err != nil {
                    log.Println (err )
                    return
                }
                
                glib.IdleAdd ( func () {
                    NotificationsListing ( MainWin.NotificationsBox, noti, ph )
                })
            }()
        case TimelineLocalPage:
            ph := PlaceHolder ( MainWin.LocalBox )
            go func () {
                toots, err := TimelineEx ( "public", false, true, 20, "", "", "" )
                if err != nil {
                    return
                }
                glib.IdleAdd ( func () {
                    TootListing ( MainWin.LocalBox, toots, ph )
                })
            }()
        case AccountPage:
            uiAccountView ( *MainWin.Account.Acc, &MainWin.Account )
    }
}

func MainWindowInit ( GtkBuilder *gtk.Builder ) (*MainWindow) {
    if MainWin != nil {
        return MainWin
    }

    MainWin = &MainWindow{}
    err := DiscoverWidgets ( GtkBuilder, MainWin, "Main" )
    if err != nil {
        log.Println ("An Error occurred on mainWindow Discovering: ", err.Error())
    }
    w, _ := MainWin.Notebook.GetNthPage( OtherAccountPage )
    w.ToWidget().Hide()
    w, _ = MainWin.Notebook.GetNthPage ( TootDetailPage )
    w.ToWidget().Hide()

    MainWin.SendBtn.Connect ("clicked", func() {
        NewTootWin.Window.Show()
    })

    MainWin.Window.Connect ( "check-resize", func() {
        h := MainWin.Window.GetAllocatedHeight()
        w := MainWin.Window.GetAllocatedWidth()

        if h != MainWin.WindowHeight || w != MainWin.WindowWidth {
            MainWin.WindowHeight = h
            MainWin.WindowWidth = w
            // resize event!
            log.Println ("Resize Fn!")

            if MainWin.Account.Acc != nil {
                f, err := cache.Get ( MainWin.Account.Acc.Header )
                if err == nil {
                    w := MainWin.Account.Header.GetAllocatedWidth ()
                    if w > MainWin.WindowWidth {
                        w = MainWin.WindowWidth - 50
                    }
                    px, err := SimpleScale ( f, w, -1 )
                    if err == nil {
                        MainWin.Account.Header.SetFromPixbuf ( px )
                    }
                }
            }
        }
    })

    MainWin.Search.Button.Connect ( "clicked", func () {
        q, err := MainWin.Search.Entry.GetText ()
        if err != nil {
            return
        }
        if q != "" {
            pltoot := PlaceHolder ( MainWin.Search.Toots )
            placc := PlaceHolder ( MainWin.Search.Accounts )
            plhash := PlaceHolder ( MainWin.Search.Hashtags )
            go func () {
                st, err := SearchStatuses ( q )
                if err != nil {
                    glib.IdleAdd ( func() {
                        pltoot.Hide()
                        pltoot.Destroy()
                        NoEntries ( MainWin.Search.Toots, "An error occurred: " + err.Error() )
                    })
                    return
                }
                log.Println ("Search resulted in: ", st )

                glib.IdleAdd ( func () {
                    if len(*st) == 0 {
                        pltoot.Hide()
                        pltoot.Destroy()
                        NoEntries ( MainWin.Search.Toots, "No toots found" )
                    } else {
                        TootListing ( MainWin.Search.Toots, *st, pltoot )
                    }
                })
            } ()

            go func () {
                accs, err := SearchAccounts ( q )
                if err != nil {
                    glib.IdleAdd ( func () {
                        placc.Hide()
                        placc.Destroy()
                        NoEntries ( MainWin.Search.Accounts, "An error occurred: "+err.Error() )
                    })
                    return
                }
                log.Println ("Search resulted in: ", accs )
                glib.IdleAdd ( func () {
                    if len(*accs) == 0 {
                        placc.Hide()
                        placc.Destroy()
                        NoEntries ( MainWin.Search.Accounts, "No Accounts found" )
                    } else {
                        AccountListing ( MainWin.Search.Accounts, *accs, nil, placc, ACCOUNT_LISTING_FOLLOWSYOU | ACCOUNT_LISTING_FOLLOW )
                    }
                })
            }()

            go func () {
                htags, err := SearchHashtags ( q )
                if err != nil {
                    glib.IdleAdd ( func() {
                        plhash.Hide()
                        plhash.Destroy()
                        NoEntries ( MainWin.Search.Hashtags, "An error occurred: "+err.Error() )
                    })
                    return
                }
                log.Println ("Search resulted in: ", htags )
                glib.IdleAdd ( func () {
                    if len(*htags) == 0 {
                        plhash.Hide()
                        plhash.Destroy()
                        NoEntries ( MainWin.Search.Hashtags, "Not implemented" )
                    } else {
                        HashtagListing ( MainWin.Search.Hashtags, *htags, plhash )
                    }
                })
            }()
        }
    })

    MainWin.Window.Connect ( "realize", func () {
        acc, err := VerifyCredentials()
        if err != nil {
            log.Println ("Ohoh! Cannot verify credentials!")
            return
        }
        MainWin.Account.Acc = acc

        MainWin.HeaderBar.SetSubtitle ( "Logged in as " + acc.DisplayName )
        MainWindowMainNotebook ( MainWin.Notebook.GetCurrentPage() )

       /* go func () {
            accs, err := Muted ()
            MuteList = []string{}
            if err == nil {
                for _, a := range accs {
                    MuteList = append ( MuteList, a.Id )
                }
            }

            accs, err = Blocked ()
            BlockList = []string{}
            if err == nil {
                for _, b := range accs {
                    BlockList = append ( BlockList, b.id )
                }
            }
        }*/
    })

    MainWin.Notebook.Connect ( "switch-page", func ( widget *gtk.Notebook, page *gtk.Widget, num int ) {
        MainWindowMainNotebook ( num )
    })

    MainWin.Window.Connect ( "close", func() {
        MainWin.Window.Hide()
        MainWin.App.Quit()
        gtk.MainQuit()
    })

    return MainWin
}
