# Go Toot!

This is a GTK-based Mastodon Client written in Go.

## Config-file

Default config file is `gotootoot.cfg` in `/` folder. The config file is created automatically if it does not exist. 

```
{
  "Token": {
    /** 
     * Token gotten from oauth2-lib is saved here
     **/
  },
  "ClientId": "<clientID>",
  "ClientSecret": "<clientSecret>",
  "Server": "<server>",
  "Scopes": [
    "read",
    "write"
  ]
}
```

## Build-Time Dependencies

* golang.org/x/oauth2
* github.com/gotk3/gotk3

Install with:

```
go get github.com/gotk3/gotk3/gtk golang.org/x/oauth2
```
